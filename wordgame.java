import java.util.Scanner;
import java.io.*;
class wordgame {
	static String[] words = { "pen", "ball", "cat", "mat", "rat"};
	static int point = 100;
	static int choice, string_length, i = 4, guess = 26;
	static char input;
	static int[] found = new int[5]; 
	static int index, count = 0;
	static String temp, blah;
	public static void display () {
		for (int j = 0; j < string_length; j++) {
			if (j == 0)
				System.out.print("|");
			if (found[j] == 0)
				System.out.print (" _ ");
			if (found [j] == 1)
				System.out.print (" " + temp.charAt(j) + " ");
			if (j == string_length - 1)
				System.out.print("|");
		}
		System.out.println("\n");
	}
	public static void main (String argv[]) {
		Scanner in = new Scanner (System.in);
		while (i > 0) {
			string_length = words[i].length();
			temp = words[i];
			System.out.println ("\nThe length of the string is: " + string_length);
			do {
				System.out.println ("1. Display score");
				System.out.println ("2. Make a guess");
				System.out.println ("3. Exit");
				System.out.print ("Enter your choice [1/2/3]: ");
				choice = in.nextInt();
					
				switch (choice) {
					case 1: 
						System.out.println ("Your score is: "  + point);
					break;
					case 2:
							if (guess < 0) {
								System.out.println ("You don't have any guesses left\n");
								System.exit(0);
							}
								
							System.out.println("No of guesses left: " + guess);
							System.out.print("Enter you guess: ");
							blah = in.next();
							input = blah.charAt(0);

							if (words[i].indexOf(input) != -1) {
								index = words[i].indexOf(input);
								if (found[index] != 1) {
									found[index]=1;
									System.out.println ("There's '" + input + "' in the word at index '" + index + "'. Congratulations! You have won 10 points!!\n");
									point = point + 10;
								}
								else
									System.out.println ("You have already given this input before");
							}
						
							else {
								System.out.println ("There's no " + input + "in the word. Sorry, I have reduced 10 points from your main points :(. Better luck next time!\n");
								point = point - 10;
							}
							guess--;
							if (point < 0) {
								System.out.println ("Oh! Man! You've lost the game! Your score is ZERO. Better luck next time!!\n");
								System.exit(0);
							}
							for (int j = 0; j < string_length; j++) {
								if (found[j] == 1)
									count++;
							}
							if (count  == string_length) {
								System.out.println ("You have won this game! Lets start the next game");
								i++;
								count=0;
								for (int j = 0; j < string_length; j++)
									found[j] = 0;
							}
							display();
							System.out.println ("value of count is: " + count + ", Value of string lenght is: " + string_length);
							count=0;
							break;
				}
			} while (choice != 3 || choice < 1 || choice > 3);
			guess = 26;
		}
	}
}
