/* ################################
	Author: Seshagiri Prabhu
	Roll No: P2CSN12028
   	A program to 
   	1. Add a new character
   	2. list the arraylist content
   	3. Remove an element
   	4. Get the size of arraylist
   	5. Get it sorted
   	6. Search for an element
################################### */
import java.io.*;
import java.util.*;

class ArrayDemo {

	static List array = new ArrayList();
	static String input;
	static Scanner in = new Scanner (System.in);
	
	/* A function to take input from user and add it to the list */
	public static void inputFunction() {
		System.out.print("So you wanna add a string huh? Please enter it here: ");
		input = in.next();
		array.add(input);
	}
	
	/* Lists all the elements in the array list */
	public static void listArray() {
		System.out.println("\n Elements in the array list: ");
		System.out.print("\t" + array);
		System.out.println();
	}
	/* Removes elements if the string is given by the user */
	public static void removeElement() {
		int index;
		System.out.println("\nThe list of elements");
		System.out.print("\t" + array);
		System.out.print("\n Enter the elements you wanna remove from the list: ");
		input = in.next();
		index = array.indexOf(input);
		array.remove(index);
	}
	
	/* Displays the size of the array */
	public static void sizeOfArray() {
		int arraySize = array.size();
		System.out.println ("Size of the array is " + arraySize + "\n");
	}
	
	/* A function to sort an array */
	public static void sortArray() {
		Collections.sort(array);
	}

	/* Searches an element according to the given search string */
	public static void searchAnElement() {
		System.out.print("Enter the element you want to search in the Array list: ");
		input = in.next();
		if (array.contains(input)) {
			System.out.println ("Element was found in Array list!\n");
		}
		else {
			System.out.println ("Element was not found in Array list:( \n");
		}
	}

	public static void main (String[] argv) {
		int choice;
		do {
			System.out.println("\n###############################");
			System.out.println("1. Add a new charecter");
			System.out.println("2. List the array-list content");
			System.out.println("3. Remove an element");
			System.out.println("4. Get the size of array list");
			System.out.println("5. Get it sorted");
			System.out.println("6. Search for an element");
			System.out.println("7. Exit");
			System.out.println("###############################\n");
			System.out.print("Enter your choice[1/2/../7]: ");
			choice = in.nextInt();
			switch (choice) {
				case 1:
						inputFunction();
					break;
				case 2:
						listArray();	
					break;
				case 3:
						removeElement();
					break;
				case 4:
						sizeOfArray();
					break;
				case 5:
						sortArray();
					break;
				case 6:
						searchAnElement();
					break;
				case 7:
						System.exit(0);
					break;
				default:
						System.out.println ("Wrong choice!! Please try again!\n");
					break;
			}

		} while (choice != 7 || choice < 1 || choice > 7);
	}
}
