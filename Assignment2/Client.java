/* ##########################
Author : Seshagiri Prabhu
Roll no: p2csn12028
file name : Client.java
Cyber Security and Networks 
############################*/


import java.io.*;
import java.util.*;
import java.net.*;

public class Client {
	static int serverPort = 6666;
	static String address = "127.0.0.1";
	static Scanner scanIn = new Scanner(System.in);

	/* Streams used to send fileName to the server */
	static InputStream sin = null;
	static OutputStream sout = null;
	static DataInputStream in = null;
	static DataOutputStream out = null;

	static Socket socket = null;
	
	static String fileName = null;
	static int fileSize = 6022386;

	static Scanner scanner = new Scanner(System.in);

	/* Streams used to recieve file from the server */
	static FileOutputStream fos = null;
	static BufferedOutputStream bos = null;
	static byte[] myByteArray = null;

	static int bytesRead;
	static int current = 0;
	static long start = 0;
	static long end = 0;

	/* A method to connect to the server */
	public static void connectToServer() {
		try {
			start = System.currentTimeMillis();

			/* Creates an object that represents the above IP address */
			InetAddress ipAddress = InetAddress.getByName(address);
			System.out.println ("Any of you heard of a socket with IP adddres " + address + "and port number " + serverPort);

			/* Creates a socket with the server's IP address and server's port */
			socket = new Socket (ipAddress, serverPort);
			System.out.println("Yes! I am connnected to the server!\n");
		}

		catch (Exception x) {
			x.printStackTrace();
		}
	}

	/* A method to send file name to the server */
	public static void sendFileName() {
		try {
			System.out.print ("Enter the file you want to retrieve from server: ");
			/* Get the input and output streams of the socket, so that you can receive and send data to the client */
			sin = socket.getInputStream();
			sout = socket.getOutputStream();
			
			/* Just converting them to different streams, so that string handling becomes easier */
			in = new DataInputStream(sin);
			out = new DataOutputStream(sout);

			fileName = scanIn.next();

			/* Send the above fileName to the server */
			out.writeUTF(fileName);
			out.flush();

			/* Wait for the server to send the same fileName back */
			fileName = in.readUTF();
			System.out.println ("You gave the file name as this '" + fileName + "'");
		}
		catch (Exception x) {
			x.printStackTrace();
		}
	}
	
	/* A method to recieve the requested file form server */
	public static void recieveFile() {
		try {
			System.out.print ("Enter the name of the file into which you want to write the data: ");
			fileName = scanner.next();
			myByteArray = new byte [fileSize];
			sin = socket.getInputStream();
			fos = new FileOutputStream (fileName);
			bos = new BufferedOutputStream(fos);
			bytesRead = sin.read (myByteArray, 0, myByteArray.length);
			current = bytesRead;

			do {
			   	bytesRead = sin.read(myByteArray, current, (myByteArray.length-current));
				if(bytesRead >= 0) {
					current += bytesRead;
				}
			} while(bytesRead > -1);

			bos.write(myByteArray, 0 , current);
			bos.flush();
			end = System.currentTimeMillis();
			System.out.println("Time elapsed in this session: " + (end - start)  + " milliseconds.");
		}

		catch (Exception x) {
			x.printStackTrace();
		}

	}
	
	/* Closes all the sockets and streams safely */
	protected void finalize() {
		try {
			socket.close();
			fos.close();
			bos.close();
			sin.close();
			out.close();
		}
		catch (IOException e) {
			System.out.println ("Couldn't close in server side!");
			System.exit(-1);
		}
	}

	public static void main (String[] args) {
		connectToServer();
		sendFileName();
		recieveFile();
	}
}

