/* ##########################
Author : Seshagiri Prabhu
Roll no : p2csn12028
File name : Server.java
Cyber Security and Networks
###########################*/
/*
Usage:
$ javac Server.java Client.java

[tty1]
$ java Server.java
Waiting for a client
Got a client

Client gave this file name: blah
Sending blah....

[tty2]
$ java Client.java
Any of you heard of a socket with IP adddres 127.0.0.1and port number 6666
Yes! I am connnected to the server!

Enter the file you want to retrieve from server: blah
You gave the file name as this 'blah'
Enter the name of the file into which you want to write the data: blahblah
Time elapsed in this session: 4483 milliseconds.
*/

import java.io.*;
import java.util.*;
import java.net.*;

public class Server {
	/* Just a random port to have fun learned from #InCTF */
	static int port = 6666;

	/* Streams used to recieve fileName from clients */
	static InputStream sin = null;
	static OutputStream sout = null;
	static DataInputStream in = null;
	static DataOutputStream out = null;

	static ServerSocket serverSocket = null;
	static Socket socket = null;
	
	static String fileName = null;
	static File myFile = null;
	static byte[] myByteArray = null;

	/* Streams used to send file to the clients */
	static FileInputStream fis = null;
	static BufferedInputStream bis = null;
	static OutputStream os = null;

	/* A method to start a passive connection */
	public static void startServer() {
		try {
			/* Create a server socket and bind it to the above port number */
			serverSocket = new ServerSocket(port);
			System.out.println ("Waiting for a client");
			
			/* Make the server listen for a connection, and let you know when it gets onemake the server listen for a connection, and let you know when it gets one */
			socket = serverSocket.accept();
			System.out.println("Got a client\n");
		}
		catch (Exception x) {
			x.printStackTrace();
		}
	}

	/* A method to get the fileName from client program */
	public static void recieveFileName() {
		try {
			/* Get the input and output streams of the socket, so that you can receive and send data to the client */
			sin = socket.getInputStream();
			sout = socket.getOutputStream();

			/*	Just converting them to different streams, so that string handling becomes easier */
			in = new DataInputStream(sin);
			out = new DataOutputStream(sout);

			/* Wait for the client to send a fileName */
			fileName = in.readUTF();
			System.out.println("Client gave this file name: " + fileName );

			/* Send the same fileName to the client */
			out.writeUTF(fileName);

			/* Flush the stream to ensure that the data reaches the other end */
			out.flush();
		}
		catch (Exception x) {
			x.printStackTrace();
		}

	}
	
	/* Sends the requested file back to the client */
	public static void sendFile() {
		try {
			myFile = new File(fileName);
			myByteArray = new byte [(int) myFile.length()];
			fis = new FileInputStream(myFile);
			bis = new BufferedInputStream(fis);
			bis.read(myByteArray, 0, myByteArray.length);
			os = socket.getOutputStream();
			System.out.println ("Sending " + fileName + "....");
			os.write(myByteArray, 0, myByteArray.length);
			os.flush();
		}
		catch (Exception x) {
			x.printStackTrace();
		}
	}

	/* Closes all the opened sockets, input and output streams */
	protected void finalize() {
		try {
			socket.close();
			out.close();
			in.close();
		}
		catch (IOException e) {
			System.out.println ("Couldn't close in client side!");
			System.exit(-1);
		}
	}
	public static void main (String[] argv) {
		startServer();
		recieveFileName();
		sendFile();
	}	
}
